# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## [2.1.2] - 2024-08-27
- MatchCreditNote resource added

## [2.1.1] - 2023-06-01
- Track microtime instead of time
- Replace uft8_decode with mb_convert_encoding.

## [2.1.0] - 2023-05-11
- Contactperson resource added

## [2.0.0] - 2021-06-23
- Breaking changes to the library.
- New namespace, and functionality.

## [0.1.0] - 2020-04-04
### Added
- MVP of the project
- Basic documentation
- This changelog


## [0.1.7] - 2020-04-04
### Added
- Support for salesinvoicelist.nv