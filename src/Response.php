<?php

namespace Kargofi\NetvisorApi;

class Response {

    public $http_status;
    public $netvisor_error;
    public $headers;
    public $is_http_successful;
    public $is_netvisor_successful;
    public $is_successful;
    public $guzzleResponse;
    public $body_xml;
    public $body_array;

    public function __construct( object $guzzleResponse ) {

        $this->guzzleResponse        = $guzzleResponse;
        $this->http_status           = $guzzleResponse->getStatusCode();
        $this->headers               = $guzzleResponse->getHeaders();
        $this->is_http_successful    = $guzzleResponse->getReasonPhrase() === 'OK';

        if ( $this->is_http_successful ) {
            $this->body_xml              = $this->get_body_xml();
            $this->body_array            = $this->get_body();
            $this->netvisor_error        = $this->set_netvisor_error();
        }
       
        $this->is_successful = $this->is_successful();
    }

    public function get_body_xml() {
        if( ! empty( $this->body_xml )  ) {
            return $this->body_xml;
        }
        $xml = $this->guzzleResponse->getBody()->getContents();
        return $xml;
    }

    public function get_body() {
        if( ! empty( $this->body_array )  ) {
            return $this->body_array;
        }
    
        $xml_to_array = new \Sabre\Xml\Service();
        $xml_to_array->elementMap = [ 
            'Root' => 'Sabre\Xml\Deserializer\keyValue',            
        ];
        $array = $xml_to_array->parse( $this->body_xml );
        return $array;
    }

    public function get_netvisor_error() {
        return $this->netvisor_error;
    }

    protected function set_netvisor_error() {
        $body = $this->body_array;
        $error = null;
        
        if( $body['{}ResponseStatus'][0]['value'] !== 'OK' ) {

            $error = [
                'status' => $body['{}ResponseStatus'][0]['value'],
                'message' => $body['{}ResponseStatus'][1]['value'],
                'timestamp' => $body['{}ResponseStatus'][2]['value'],
            ];
            
        }
        return $error;
    }

    public function is_successful() {
        return  $this->is_http_successful && empty( $this->netvisor_error );
    }

    public function is_http_successful() {
        return $this->is_http_successful;
    }

    public function get_guzzle_response() {
        return $this->guzzleResponse;
    }

}