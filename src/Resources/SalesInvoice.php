<?php

namespace Kargofi\NetvisorApi\Resources;

class SalesInvoice extends Resource {

    public function __construct( $parameters = [] ) {
        $this->http_method = 'POST';
        $this->netvisor_name = 'salesinvoice.nv';
        $this->parameters = $parameters;
    }
    
}