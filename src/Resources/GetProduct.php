<?php

namespace Kargofi\NetvisorApi\Resources;

class GetProduct extends Resource {

    public function __construct( $parameters = [] ) {
        $this->http_method = 'GET';
        $this->netvisor_name = 'getproduct.nv';
        $this->parameters = $parameters;
    }
    
}