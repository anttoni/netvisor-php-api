<?php

namespace Kargofi\NetvisorApi\Resources;

class SalesPayment extends Resource {

    public function __construct( $parameters = [] ) {
        $this->http_method = 'POST';
        $this->netvisor_name = 'salespayment.nv';
        $this->parameters = $parameters;
    }
    
}