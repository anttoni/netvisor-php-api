<?php

namespace Kargofi\NetvisorApi\Resources;

class Customer extends Resource {

    public function __construct( $parameters = [] ) {
        $this->http_method = 'POST';
        $this->netvisor_name = 'customer.nv';
        $this->parameters = $parameters;
    }
    
}