<?php

namespace Kargofi\NetvisorApi\Resources;

class DeleteSalesPayment extends Resource
{

    public function __construct($parameters = [])
    {
        $this->http_method = 'GET';
        $this->netvisor_name = 'deletesalespayment.nv';
        $this->parameters = $parameters;
    }
}
