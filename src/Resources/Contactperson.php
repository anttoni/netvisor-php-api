<?php

namespace Kargofi\NetvisorApi\Resources;

class Contactperson extends Resource
{

    public function __construct($parameters = [])
    {
        $this->http_method = 'POST';
        $this->netvisor_name = 'contactperson.nv';
        $this->parameters = $parameters;
    }
}
