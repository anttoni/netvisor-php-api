<?php

namespace Kargofi\NetvisorApi\Resources;

class Product extends Resource {

    public function __construct( $parameters = [] ) {
        $this->http_method = 'POST';
        $this->netvisor_name = 'product.nv';
        $this->parameters = $parameters;
    }
    
}