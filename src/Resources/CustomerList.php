<?php

namespace Kargofi\NetvisorApi\Resources;

class CustomerList extends Resource {
    
    public function __construct( $parameters = [] ) {
        $this->http_method = 'GET';
        $this->netvisor_name = 'customerlist.nv';
        $this->parameters = $parameters;
    }
    
}