<?php

namespace Kargofi\NetvisorApi\Resources;

class ProductList extends Resource {

    public function __construct( $parameters = [] ) {
        $this->http_method = 'GET';
        $this->netvisor_name = 'productlist.nv';
        $this->parameters = $parameters;
    }
    
}