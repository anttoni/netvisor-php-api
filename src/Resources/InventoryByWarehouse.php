<?php

namespace Kargofi\NetvisorApi\Resources;

class InventoryByWarehouse extends Resource {

    public function __construct( $parameters = [] ) {
        $this->http_method = 'GET';
        $this->netvisor_name = 'inventorybywarehouse.nv';
        $this->parameters = $parameters;
    }
}