<?php

namespace Kargofi\NetvisorApi\Resources;

class GetSalesinvoice extends Resource {

    public function __construct( $parameters = [] ) {
        $this->http_method = 'GET';
        $this->netvisor_name = 'getsalesinvoice.nv';
        $this->parameters = $parameters;
    }
}