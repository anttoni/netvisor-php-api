<?php

namespace Kargofi\NetvisorApi\Resources;

class ExtendedProductList extends Resource {

    public function __construct( $parameters = [] ) {
        $this->http_method = 'GET';
        $this->netvisor_name = 'extendedproductlist.nv';
        $this->parameters = $parameters;
    }
    
}