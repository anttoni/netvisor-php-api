<?php

namespace Kargofi\NetvisorApi\Resources;

class GetInventoryPlaces extends Resource
{

    public function __construct($parameters = [])
    {
        $this->http_method = 'GET';
        $this->netvisor_name = 'getinventoryplaces.nv';
        $this->parameters = $parameters;
    }
}
