<?php

namespace Kargofi\NetvisorApi\Resources;

class Resource
{

    protected $http_method;

    protected $parameters;

    protected $body;

    protected $netvisor_name;

    protected $transaction_identifier;

    public function __construct(string $http_method, string $netvisor_name, array $parameters = [])
    {
        $this->netvisor_name = $netvisor_name;
        $this->http_method = $http_method;
        $this->parameters = $parameters;
    }

    public function append($request_addr)
    {
        return $request_addr . $this->netvisor_name . $this->get_parameters();
    }

    public function http_method()
    {
        return $this->http_method;
    }

    public function get_name()
    {
        return $this->netvisor_name;
    }

    public function get_parameters()
    {
        if (empty($this->parameters)) {
            return '';
        }
        return '?' . urldecode(http_build_query($this->parameters, '', '&'));
    }

    public function get_transaction_identifier()
    {
        if (!$this->transaction_identifier) {
            $this->transaction_identifier = 'Kargofi\API\\' . $this->get_name() . '\\' . microtime(true) . '\\' . hash('sha256', implode('+', $this->parameters));
        }
        return $this->transaction_identifier;
    }

    public function get_body()
    {
        if ($this->http_method() === 'GET') {
            trigger_error('GET resource doesn\'t have body.', E_USER_ERROR);
        }
        return $this->body;
    }

    public function set_body($body)
    {
        if ($this->http_method() === 'GET') {
            trigger_error('GET resource cannot have body.', E_USER_ERROR);
        }
        $this->body = $body;
    }
}
