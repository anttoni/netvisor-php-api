<?php

namespace Kargofi\NetvisorApi\Resources;

class GetCustomer extends Resource {

    public function __construct( $parameters = [] ) {
        $this->http_method = 'GET';
        $this->netvisor_name = 'getcustomer.nv';
        $this->parameters = $parameters;
    }
}