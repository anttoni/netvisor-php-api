<?php

namespace Kargofi\NetvisorApi\Resources;

class UpdateSalesInvoiceStatus extends Resource {
    
    public function __construct( $parameters = [] ) {
        $this->http_method = 'POST';
        $this->netvisor_name = 'updatesalesinvoicestatus.nv';
        $this->parameters = $parameters;
    }
    
}