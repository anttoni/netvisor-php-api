<?php

namespace Kargofi\NetvisorApi\Resources;

class MatchCreditNote extends Resource
{

    public function __construct($parameters = [])
    {
        $this->http_method = 'POST';
        $this->netvisor_name = 'matchcreditnote.nv';
        $this->parameters = $parameters;
    }
}
