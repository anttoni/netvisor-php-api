<?php

namespace Kargofi\NetvisorApi\Resources;

class SalesPaymentList extends Resource {

    public function __construct( $parameters = [] ) {
        $this->http_method = 'GET';
        $this->netvisor_name = 'salespaymentlist.nv';
        $this->parameters = $parameters;
    }
    
}