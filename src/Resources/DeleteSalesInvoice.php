<?php

namespace Kargofi\NetvisorApi\Resources;

class DeleteSalesInvoice extends Resource
{

    public function __construct($parameters = [])
    {
        $this->http_method = 'GET';
        $this->netvisor_name = 'deletesalesinvoice.nv';
        $this->parameters = $parameters;
    }
}
