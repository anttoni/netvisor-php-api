<?php

namespace Kargofi\NetvisorApi\Resources;

class SalesInvoiceList extends Resource {

    public function __construct( $parameters = [] ) {
        $this->http_method = 'POST';
        $this->netvisor_name = 'salesinvoicelist.nv';
        $this->parameters = $parameters;
    }
    
}