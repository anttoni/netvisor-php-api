<?php

namespace Kargofi\NetvisorApi\Resources;

class WarehouseEvent extends Resource {

    public function __construct( $parameters = [] ) {
        $this->http_method = 'POST';
        $this->netvisor_name = 'warehouseevent.nv';
        $this->parameters = $parameters;
    }
}