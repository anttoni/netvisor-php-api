<?php
namespace Kargofi\NetvisorApi;

class Helpers {

    /**
     * Check that necessary arguments exist.
     *
     * @param array $args
     * @return void
     */
    public static function args_check( $args, $required_args, $function) {
        foreach( $required_args as $requirement ) {
            if (  ! array_key_exists( $requirement, $args ) ) {
                trigger_error('Missing required argument' . $requirement . ' in ' . $function , E_USER_ERROR );
            }
        }
    }

    /**
     * Writes array to XML with sabre/xml. See array stuctures: https://sabre.io/xml/writing/.
     * Removes <?xml tag since Netvisor doesn't accept it
     * Unprefixes data if it's prefixed to prevent sabre/xml extended syntax collisions.
     *
     * @param array $data Array to write.
     * @param string|null $prefix Prefix if not omitted.
     * @return void
     */
    public static function data_to_xml( $data) {
        $xml = self::array_to_xml( $data );
        $xml = self::remove_xml_header( $xml );
        return $xml;
    }

    /**
     * Change array to xml using Sabre/xml. 
     * See supported array structures: https://sabre.io/xml/writing/
     * tl;dr: If you need to use 'name' attribute, prefix it or use 
     * ['name => '', ''value => ''] structures.
     * 
     * @param array $data Array that sabre supports
     * @return string $xml XML string 
     */
    public static function array_to_xml( $data ) {
        $array_to_xml = new \Sabre\Xml\Service();
        $xml = $array_to_xml->write( 'root', $data );
        return $xml;
    }

    /**
     * Remove header from XML since Sabre creates it and netvisor doesn't accept it.
     *
     * @param string $xml XML with <?xml> tag.
     * @return string $xml XML without <?xml> tag.
     */
    public static function remove_xml_header( string $xml ) {
        $xml = substr( $xml, strpos($xml, '?'.'>') + 2);
        return $xml;
    }
}