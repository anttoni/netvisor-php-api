<?php

namespace Kargofi\NetvisorApi;

class Client extends \GuzzleHttp\Client
{

    private $base_uri;
    private $options_sender;
    private $auth_user_id;
    private $auth_user_key;
    private $auth_partner_id;
    private $auth_partner_key;
    private $options_language;
    private $options_organization_identifier;

    public function __construct(
        string $base_uri,
        array $netvisor_args = [],
        array $guzzle_args = []
    ) {

        $this->base_uri = $base_uri;

        $required_args = [
            'options_sender',
            'auth_user_id',
            'auth_user_key',
            'auth_partner_id',
            'auth_partner_key',
            'options_language',
            'options_organization_identifier',
        ];

        Helpers::args_check($netvisor_args, $required_args, __FUNCTION__);

        foreach ($required_args as $variable) {
            $this->$variable = $netvisor_args[$variable];
        }

        if (empty($guzzle_args)) {
            $guzzle_args = ['base_uri' => $base_uri, 'timeout' => 5];
        }

        parent::__construct($guzzle_args);
    }

    public function nv_get(object $resource)
    {

        $options = [];
        $options['headers'] = $this->build_auth_headers($resource);
        if ($resource->http_method() === 'POST') {
            $options['body'] = Helpers::data_to_xml($resource->get_body());
        }

        $response = $this->request(
            $resource->http_method(),
            $resource->append($this->base_uri),
            $options,
        );

        return new Response($response);
    }

    private function build_auth_headers($resource)
    {
        $headers = [
            'X-Netvisor-Authentication-Sender'            => $this->options_sender,
            'X-Netvisor-Authentication-CustomerId'        => $this->auth_user_id,
            'X-Netvisor-Authentication-PartnerId'        => $this->auth_partner_id,
            'X-Netvisor-Authentication-Timestamp'        => $this->get_time_stamp(),
            'X-Netvisor-Authentication-TransactionId'    => $resource->get_transaction_identifier(),
            'X-Netvisor-Interface-Language'                => $this->options_language,
            'X-Netvisor-Organisation-ID'                => $this->options_organization_identifier,
            'X-Netvisor-Authentication-MAC'                => $this->get_MAC($resource),
            'X-Netvisor-Authentication-MACHashCalculationAlgorithm' => 'SHA256',
        ];

        return $headers;
    }

    private function get_MAC($resource)
    {

        $parameters = [
            // API uses ISO-8859-1 encoding, but not for commas.
            mb_convert_encoding($resource->append($this->base_uri), 'UTF-8',  'ISO-8859-1'),
            $this->options_sender,
            $this->auth_user_id,
            $this->get_time_stamp(),
            $this->options_language,
            $this->options_organization_identifier,
            $resource->get_transaction_identifier(),
            $this->auth_user_key,
            $this->auth_partner_key,
        ];

        $sha = hash('sha256', implode('&', $parameters));

        return $sha;
    }

    private function get_time_stamp()
    {
        $time = date('Y-m-d H:i:s');
        return $time;
    }
}
