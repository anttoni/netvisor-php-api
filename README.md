# Netvisor PHP API v2

A PHP Library for making requests to Netvisor API. Still on its early stages.

> See Netvisor's apidoc at: [support.netvisor.fi](https://support.netvisor.fi/hc/fi/articles/200874928-Netvisor-API-basics-and-resources) (both in English and in Finnish)

In order to use this, you need Netvisor account. Then see [instructions for enabling API.](https://support.netvisor.fi/hc/fi/articles/203082606-Web-Service-Interface-identifiers-activation-and-pricing)

This library uses [sabre/xml](https://sabre.io/xml/) for writing and reading XML and [Guzzle's PSR7 Request](http://docs.guzzlephp.org/en/stable/psr7.html) for making HTTP-requests.

This documentation and the library is WIP. Refer to [example](https://gitlab.com/anttoni/netvisor-php-api/-/blob/master/example.php) for some currently supported examples.

## Installation

Add GitLab repository into your composer.json
```
    "repositories": [
        {
            "type": "git",
            "url": "https://gitlab.com/anttoni/netvisor-php-api"
        }
```

Require library from command line

```
$ composer require kargofi/netvisor-api
``` 

## Usage

### Create Kargofi\NetvisorApi\Client
Create `Client` that handles authorization and making the requests. Constructor takes three arguments. 

#### `$base_uri` *(required)*
Base url of Netvisor environment. For test environments this is usually `https://isvapi.netvisor.fi`, for production `https://integration.netvisor.fi`.   

#### `$netvisor_args` *(required)*
Each request containts authorization headers that are build for you automatically in `Client::build_auth_headers` built from `$netvisor_args` which is an associative array containing:
-  (string) `'options_sender'` - Some sender identification
-  (string) `'auth_user_id'` - User ID from Netvisor
-  (string) `'auth_user_key'` - User key from Netvisor
-  (string) `'auth_partner_id'` - Partner ID obtainable from Netvisor Support[(read more)](https://support.netvisor.fi/en/support/solutions/articles/77000466610-fundamentals-of-web-service-interface)
-  (string) `'auth_partner_key'` - Partner key obtainable from Netvisor Support 
-  (string) `'options_language'` - FI for finnish, SE for swedish, EN for english 
-  (string) `'options_organization_identifier'` - VAT ID of the company

#### `$guzzle_args` *(optional)*
`Kargofi\NetvisorApi\Client` extends `GuzzleHttp\Client`. This means that all the functionality of Guzzle Client can be utilized. You can pass any arguments needed. If this is left empty `base_uri` and `timeout` are passed.
```
  $guzzle_args = [ 'base_uri' => $base_uri, 'timeout' => 5 ];
```
If you pass anything, make sure to include `base_uri` to the array as well.

### Create resource and pass it to `Client::get()`
To make requests create `Kargofi\NetvisorApi\Resources\Resource`. 

For example in order to list available customers, create new `Kargofi\NetvisorApi\Resources\CustomerList`. GET resources do not necessarily need any parameters. 

To retrieve that resource use `$client->get($resource)`.