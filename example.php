<?php
use Kargofi\NetvisorApi\Helpers;
use Kargofi\NetvisorApi\Client;
use Kargofi\NetvisorApi\Resources\Customer;
use Kargofi\NetvisorApi\Resources\CustomerList;

require_once __DIR__ . '/vendor/autoload.php';

function login() {
	return [
		'options_sender'					=> 'Sender',
		'auth_user_id'						=> 'XX_123456_12345',
		'auth_user_key'						=> 'user_key',
		'auth_partner_id'					=> 'Par_ID000',
		'auth_partner_key'					=> 'partner_key',
		'options_language'					=> 'FI',
		'options_organization_identifier'	=> '1234567-8',
	];
}

/**
 * List existing customers
 * 
 * See apidoc (in Finnish): https://support.netvisor.fi/hc/fi/articles/201956217-Resurssit-Asiakasrekisteri#Asiakaslistan_nouto
 * 
 * @return void
 */
function customerlist() {

	$resource = new CustomerList();
	$client = new Client( 'https://isvapi.netvisor.fi', login() );

	$response = $client->get( $resource );

	$xml = $response->get_body_xml();
	$array = $response->get_body();
}

/**
 * Create new customer
 * 
 * See apidoc (in Finnish): https://support.netvisor.fi/hc/fi/articles/201956217-Resurssit-Asiakasrekisteri#Asiakastietojen_tuonti
 *
 * @return array $array Contains api response,
 */
function customer() {
	$data = [
		'customer' => [
			'customerbaseinformation' => [
				'prefix_name' => 'Customer Name',
				'streetaddress' => 'Address 1 AB, Locationcity',
			],
		],
	];

	$parameters = [
		'method'	=> 'add',
		'id'		=> '1',
	];

	$data = Helpers::just_give_me_xml( $data, 'prefix_' );

	$resource = new Customer();
	$resource->set_body( $data );

	$client = new Client( 'https://isvapi.netvisor.fi', login(), $parameters  );

	$response = $client->get( $resource );

	$xml = $response->get_body_xml();
	$array = $response->get_body();

	return $array;

}


customerlist();